# obs-source-transition
This is a simple OBS python plugin that allows any number of sources to be faded in and out over a specified duration and after a specified delay when activated.

Installation:
1. Install python 3.6.6 64-bit
2. In the scripts window in OBS, on the python tab, link to the installed Python directory (the folder containing python.exe)
3. In the scripts window in OBS, on the scripts tab, add the obs-source-fade-in scripts.
